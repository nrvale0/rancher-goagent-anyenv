#!/bin/bash

set -e

echo -n "Installing pyenv..."
anyenv install -v pyenv
echo "done."

eval "$(anyenv init -)"
pyenv install -v 3.4.3 && pyenv global 3.4.3

pyenv versions
