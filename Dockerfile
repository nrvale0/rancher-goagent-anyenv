FROM docker:1.10.3-dind

MAINTAINER Nathan Valentine <nrvale0@gmail.com>

ENV PATH /root/.anyenv/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/usr/sbin:/bin:/sbin
ENV TERM ansi
# ENV GIT_SSH_COMMAND 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no'

RUN apk update && apk upgrade

RUN apk add alpine-sdk build-base linux-headers openssl-dev readline-dev \
	zlib-dev bzip2-dev sqlite-dev ruby-dev libc-dev postgresql-dev \
	libxml2-dev libxslt-dev python-dev bash

RUN git clone -v https://github.com/riywo/anyenv ~/.anyenv && mkdir -p ~/.anyenv/envs

RUN echo 'export PATH="$HOME/.anyenv/bin:$PATH"' >> /etc/profile.d/anyenv.sh && \
	echo 'eval "$(anyenv init -)"' >> /etc/profile.d/anyenv.sh && \
	chmod +x /etc/profile.d/anyenv.sh

ADD anyenv-build-rbenv.bash /tmp
RUN (cd /tmp; chmod +x anyenv-build-rbenv.bash && sleep 1 && ./anyenv-build-rbenv.bash)

ADD anyenv-build-pyenv.bash /tmp
RUN (cd /tmp; chmod +x anyenv-build-pyenv.bash && sleep 1 && ./anyenv-build-pyenv.bash)

ADD anyenv-build-goenv.bash /tmp
RUN (cd /tmp; chmod +x anyenv-build-goenv.bash && sleep 1 && ./anyenv-build-goenv.bash)

RUN (cd /tmp; rm -rf *)
