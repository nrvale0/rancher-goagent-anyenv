#!/bin/bash

set -e

echo -n "Installing goenv..."
anyenv install goenv
echo "done."

eval "$(anyenv init -)"
goenv install 1.6 && goenv global 1.6

goenv versions
