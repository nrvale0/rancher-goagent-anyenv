#!/bin/bash

set -e

echo -n "Installing rbenv..."
anyenv install -v rbenv
echo "done."

eval "$(anyenv init -)"
rbenv install -v 2.3.0 && rbenv global 2.3.0

rbenv versions
